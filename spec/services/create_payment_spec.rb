require "rails_helper"

RSpec.describe CreatePayment do
  let(:valid_attributes) do
    {
      "description" => "An invoice request",
      "amount_cents" => 10_000,
      "amount_currency" => "USD",
      "id" => "123"
    }
  end
  describe "validations" do
    it "is invalid without a description" do
      expect(described_class.new(valid_attributes.except("description"))).not_to be_valid
    end

    it "is invalid without an amount" do
      expect(described_class.new(valid_attributes.except("amount_cents"))).not_to be_valid
    end

    it "is invalid without an amount currency" do
      expect(described_class.new(valid_attributes.except("amount_currency"))).not_to be_valid
    end

    it "is invalid with unsupported currency" do
      expect(described_class.new(valid_attributes.merge("amount_currency" => "PLN"))).not_to be_valid
    end

    it "is invalid without an invoice reference id" do
      expect(described_class.new(valid_attributes.except("id"))).not_to be_valid
    end

    it "is valid with required attributes" do
      expect(described_class.new(valid_attributes)).to be_valid
    end

  end
end
