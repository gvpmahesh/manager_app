require 'rails_helper'
require 'sidekiq/testing'

describe RejectPayment, type: :service do
  let!(:payment) { create(:payment) }

  describe '#perform!' do
    before do
      travel_to DateTime.parse("2022-10-9 10:00:00")
    end

    it "sets the payment's rejected_at to the current time" do
      expect(payment.rejected_at).to be_nil

      described_class.new(payment).perform!

      expect(payment.rejected_at).to eq(DateTime.parse("2022-10-9 10:00:00"))
    end

    it "creates an event with the payment's attributes" do
      described_class.new(payment).perform!

      event = Event.find_by(payment_id: payment.id)
      expect(event.name).to eq('payment_rejected')
      expect(event.payload.as_json).to eq(payment.attributes.as_json)
    end

    it "enqueues a job to process the event" do
      allow(RejectPaymentJob).to receive(:perform_async)
      described_class.new(payment).perform!

      event = Event.find_by(payment_id: payment.id)
      expect(RejectPaymentJob).to have_received(:perform_async).with(event.id)
    end

    it "returns a success message" do
      result = described_class.new(payment).perform!

      expect(result[:success]).to be_truthy
      expect(result[:message]).to eq('Payment rejected')
    end

    context "when there is an error" do
      it "returns an error message" do
        allow(payment).to receive(:save!).and_raise(StandardError)

        result = described_class.new(payment).perform!

        expect(result[:success]).to be_falsey
        expect(result[:message]).to eq('unable to reject payment, contact admin')
      end
    end

    context "when the payment is already rejected" do
      it "returns an error message" do
        payment.update(rejected_at: DateTime.now)

        result = described_class.new(payment).perform!

        expect(result[:success]).to be_falsey
        expect(result[:message]).to eq('Payment already rejected')
      end
    end

    context "when the payment is already approved" do
      it "returns an error message" do
        payment.update(approved_at: DateTime.now)

        result = described_class.new(payment).perform!

        expect(result[:success]).to be_falsey
        expect(result[:message]).to eq('Payment already approved')
      end
    end
  end
end
