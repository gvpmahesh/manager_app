require 'rails_helper'
require 'sidekiq/testing'

RSpec.describe InvoiceRaisedJob, type: :job do
  before do
    travel_to Time.now
  end

  after do
    travel_back
  end

  describe "#perform" do
    let(:payload) {
      "{\"id\":34,\"created_at\":\"2022-10-10T06:08:55.831Z\",\"payment_id\":null,\"updated_at\":\"2022-10-10T06:08:55.831Z\",\"approved_at\":null,\"description\":\"cool\",\"rejected_at\":null,\"amount_cents\":900000,\"amount_currency\":\"EUR\"}"
    }

    it "Creates a payment and an event with the correct attributes" do
      Sidekiq::Testing.inline! do
        described_class.perform_async(payload)

        payment = Payment.last
        event = Event.last

        expect(payment.invoice_reference_id).to eq(34)
        expect(payment.description).to eq("cool")
        expect(payment.amount_cents).to eq(900000)
        expect(payment.amount_currency).to eq("EUR")

        expect(event.payment_id).to eq(payment.id)
        expect(event.payload).to eq(JSON.parse(payload))
        expect(event.name).to eq("invoice_raised")
      end
    end
  end
end
