require 'rails_helper'
require 'sidekiq/testing'

RSpec.describe RejectPaymentJob, type: :job do
  before do
    travel_to Time.now
  end

  after do
    travel_back
  end

  let!(:payment) { create(:payment, approved_at: DateTime.current) }
  let!(:event) { create(:event, name: 'payment_approved', payment_id: payment.id, payload: payment.attributes) }

  # "{\"data\":{\"id\":56,\"name\":\"payment_approved\",\"payload\":{\"id\":82,\"rejected_at\":\"2022-10-09T13:22:10.000Z\",\"approved_at\":null,\"invoice_reference_id\":1,\"description\":\"request for invoice\",\"amount_cents\":1,\"amount_currency\":\"USD\",\"created_at\":\"2022-10-09T13:22:10.000Z\",\"updated_at\":\"2022-10-09T13:22:10.000Z\"},\"payment_id\":82,\"created_at\":\"2022-10-09T13:22:10.000Z\",\"updated_at\":\"2022-10-09T13:22:10.000Z\"}}"

  it "writes to kafka topic if cache is not set" do
    Sidekiq::Testing.inline! do
      expect(Karafka.producer).to receive(:produce_sync)
      described_class.perform_async(event.id)
    end
  end

  it "does not write to kafka topic if cache is set" do
    Sidekiq::Testing.inline! do
      $redis.set("payment_rejected:#{event.id}", 1)
      expect(Karafka.producer).not_to receive(:produce_sync)

      described_class.perform_async(event.id)
    end
  end
end
