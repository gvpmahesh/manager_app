FactoryBot.define do
  factory :payment do
    approved_at { nil }
    rejected_at { nil }
    invoice_reference_id { 1 }
    description { "request for invoice" }
    amount_cents { 1 }
    amount_currency { "USD" }
  end
end
