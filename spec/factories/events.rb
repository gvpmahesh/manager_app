FactoryBot.define do
  factory :event do
    name { "MyString" }
    payload { {} }
    payment_id { create(:payment).id }
  end
end
