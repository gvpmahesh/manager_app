require "rails_helper"

RSpec.describe Payment, type: :model do
  describe "validations" do
    it "is invalid without an invoice reference" do
      payment = build(:payment, invoice_reference_id: nil)
      expect(payment).to be_invalid
    end

    it "is invalid without a description" do
      payment = build(:payment, description: nil)
      expect(payment).to be_invalid
    end

    it "is invalid without an amount" do
      payment = build(:payment, amount_cents: nil)
      expect(payment).to be_invalid
    end

    it "is invalid without a currency" do
      payment = build(:payment, amount_currency: nil)
      expect(payment).to be_invalid
    end
  end
end
