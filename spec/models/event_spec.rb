require 'rails_helper'

RSpec.describe Event, type: :model do
  describe "validations" do
    it "has a valid factory" do
      expect(build(:event)).to be_valid
    end

    it "is invalid without a name" do
      event = build(:event, name: nil)
      expect(event).to be_invalid
    end

    it "is invalid without a payment_id" do
      event = build(:event, payment_id: nil)
      expect(event).to be_invalid
    end

    it "validates the uniqueness with respect to payment_id" do
      event = create(:event, name: 'payment_approved')
      expect(build(:event, name: 'payment_approved', payment_id: event.payment_id)).to be_invalid
    end
  end
end
