require 'rails_helper'

RSpec.describe PaymentsHelper, type: :helper do
  describe "#status" do
    it "returns 'approved' if payment is approved" do
      payment = Payment.new(approved_at: Time.now)
      expect(helper.status(payment)).to eq('approved')
    end

    it "returns 'rejected' if payment is rejected" do
      payment = Payment.new(rejected_at: Time.now)
      expect(helper.status(payment)).to eq('rejected')
    end

    it "returns 'pending for approval' if payment is pending" do
      payment = Payment.new
      expect(helper.status(payment)).to eq('pending for approval')
    end
  end
end
