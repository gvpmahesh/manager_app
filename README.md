# README

A Simple Manager App to approve/reject Invoice requests.

### Dependencies

- ruby-3.0.0
- Postgres(latest)
- sidekiq
- redis
- kafka


### Running the app

```
bundle exec sidekiq
bundle exec karafka server
rails s -p 3001
```

### Assumptions made

- This is not a multi-tenant application.
- The manager is the only user of the application.
- The manager can only approve/reject invoices for the same contractor.
- Authentication/Authorization is not implemented.
- Cannot approve the invoice once rejected.
- Cannot reject the invoice once approved.
