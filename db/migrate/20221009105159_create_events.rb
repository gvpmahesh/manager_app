class CreateEvents < ActiveRecord::Migration[7.0]
  def change
    create_table :events do |t|
      t.string :name, null: false
      t.jsonb :payload, default: {}, null: false
      t.references :payment, null: false, foreign_key: true

      t.timestamps
    end

    add_index :events, [:payment_id, :name], unique: true
  end
end
