class CreatePayments < ActiveRecord::Migration[7.0]
  def change
    create_table :payments do |t|
      t.datetime :approved_at
      t.datetime :rejected_at
      t.integer :invoice_reference_id, null: false
      t.text :description, null: false
      t.bigint :amount_cents, null: false
      t.string :amount_currency, null: false

      t.timestamps
    end

    add_index :payments, :invoice_reference_id, unique: true
  end
end
