redis_config = Rails.application.config_for(:redis)

Sidekiq.configure_server do |config|
  config.redis = { url: "redis://127.0.0.1:6379/#{redis_config['sidekiq_database']}" }
end

Sidekiq.configure_client do |config|
  config.redis = { url: "redis://127.0.0.1:6379/#{redis_config['sidekiq_database']}" }
end
