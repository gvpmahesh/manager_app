Rails.application.routes.draw do
  resources :payments, only: [:index] do
    member do
      put 'approve', to: 'payment_approval#perform'
      put 'reject', to: 'payment_rejection#perform'
    end
  end

  root "payments#index"
end
