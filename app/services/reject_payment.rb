class RejectPayment
  attr_reader :payment

  def initialize(payment)
    @payment = payment
  end

  def perform!
    return { success: false, message: 'Payment already approved' } if payment.approved_at
    return { success: false, message: 'Payment already rejected' } if payment.rejected_at

    ApplicationRecord.transaction do
      payment.rejected_at = Time.now
      payment.save!
      event = Event.create!(name: 'payment_rejected', payload: payment.attributes, payment_id: payment.id)
      RejectPaymentJob.perform_async(event.id)
    end
    { success: true, message: 'Payment rejected' }
  rescue => e
    Rails.logger.error(e)
    { success: false, message: 'unable to reject payment, contact admin' }
  end
end
