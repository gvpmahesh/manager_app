class CreatePayment
  include ActiveModel::Validations

  validates :description, presence: true

  validates :amount, presence: true, numericality: { greater_than: 0 }

  validates :amount_currency, presence: true, inclusion: { in: %w[USD EUR] }

  validates :invoice_reference_id, presence: true

  attr_reader :description, :amount_cents, :amount_currency, :invoice_reference_id

  def initialize(data)
    @description = data['description']
    @amount_cents = data['amount_cents']
    @amount_currency = data['amount_currency']
    @invoice_reference_id = data['id']
  end

  def save!
    Payment.create!(
      invoice_reference_id: invoice_reference_id,
      description: description,
      amount: amount
    )
  end

  private

  def amount
    Money.new(amount_cents, amount_currency)
  end
end
