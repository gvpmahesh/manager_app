class PaymentApprovalController < ApplicationController
  def perform
    result = ApprovePayment.new(payment).perform!

    flash[:notice] = result[:message]
    redirect_to payments_path
  end

  private

  def payment
    Payment.find(params[:id])
  end
end
