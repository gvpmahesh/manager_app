class PaymentRejectionController < ApplicationController
  def perform
    result = RejectPayment.new(payment).perform!

    flash[:notice] = result[:message]
    redirect_to payments_path
  end

  private

  def payment
    Payment.find(params[:id])
  end
end
