class Event < ApplicationRecord
  validates :name, presence: true
  validates :payment_id, presence: true
  validates :name, uniqueness: { scope: :payment_id }
end
