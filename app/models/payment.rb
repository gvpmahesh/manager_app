class Payment < ApplicationRecord
  validates :invoice_reference_id, presence: true
  validates :description, presence: true
  validates :amount_currency, presence: true

  monetize :amount_cents, allow_nil: false

  validates :invoice_reference_id, uniqueness: true
end
