class InvoiceRaisedConsumer < ApplicationConsumer
  def consume
    messages.each do |message|
      payload = message.payload['data']

      InvoiceRaisedJob.perform_async(payload.to_json)

      mark_as_consumed(message)
    end
  end

  # Run anything upon partition being revoked
  # def revoked
  # end

  # Define here any teardown things you want when Karafka server stops
  # def shutdown
  # end
end
