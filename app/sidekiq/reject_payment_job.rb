class RejectPaymentJob
  include Sidekiq::Job

  def perform(event_id)
    event = Event.find(event_id)

    return if cache_set?(event)

    Karafka.producer.produce_sync(topic: 'payment_rejected', payload: {data: event.attributes}.to_json)

    set_cache(event)
  end

  private

  def set_cache(event)
    $redis.set("payment_rejected:#{event.id}", 1)
  end

  def cache_set?(event)
    $redis.get("payment_rejected:#{event.id}").present?
  end
end
