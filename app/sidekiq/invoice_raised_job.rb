class InvoiceRaisedJob
  include Sidekiq::Job


  def perform(payload)
    payload = JSON.parse(payload)

    ApplicationRecord.transaction do
      payment = CreatePayment.new(payload).save!
      Event.create!(payment_id: payment.id, payload: payload, name: 'invoice_raised')
    end
  end
end
