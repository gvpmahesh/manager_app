class ApprovePaymentJob
  include Sidekiq::Job

  def perform(event_id)
    event = Event.find(event_id)

    return if cache_set?(event)

    Karafka.producer.produce_sync(topic: 'payment_approved', payload: {data: event.attributes}.to_json)

    set_cache(event)
  end

  private

  def set_cache(event)
    $redis.set("payment_approved:#{event.id}", 1)
  end

  def cache_set?(event)
    $redis.get("payment_approved:#{event.id}").present?
  end
end
