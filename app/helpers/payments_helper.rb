module PaymentsHelper
  def status(payment)
    if payment.approved_at
      'approved'
    elsif payment.rejected_at
      'rejected'
    else
      'pending for approval'
    end
  end

  def can_approve?(payment)
    !payment.approved_at && !payment.rejected_at
  end

  def can_reject?(payment)
    !payment.approved_at && !payment.rejected_at
  end
end
